﻿(function(){
	'use strict';

	var mediaStream = null;
	var currentCam  = null;
	var photoReady  = false;

	// CSS filters 
	var index     = 0;

	
	var r11_e;
	var r12_e;
	var r13_e;
	var r21_e;
	var r22_e;
	var r23_e;
	var r31_e;
	var r32_e;
	var r33_e;

	var video;

	var canvas;
	
	var canvas_examples;
	var savedfilters;

	var iter = 0;

	var init = function () {
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

		video = document.getElementById('videoTag');
		video.addEventListener('canplay', capture, false);
		video.addEventListener('canplay', show_saved_filteres, false);
		
		//document.getElementById('change-vid-filters').addEventListener('click', changeCssFilterOnVid, false);
		document.getElementById('change-examp_filters').addEventListener('click', changeExampFilter, false);
		document.getElementById('set-examp_filters').addEventListener('click', setExampFilter, false);
		document.getElementById('delete-examp_filters').addEventListener('click', deleteExampFilter, false);

		//document.getElementById('change-vid-filters').addEventListener('play', capture , false);
		document.getElementById('r11').addEventListener('input', getVal, false);
		document.getElementById('r12').addEventListener('input', getVal, false);
		document.getElementById('r13').addEventListener('input', getVal, false);
		document.getElementById('r21').addEventListener('input', getVal, false);
		document.getElementById('r22').addEventListener('input', getVal, false);
		document.getElementById('r23').addEventListener('input', getVal, false);
		document.getElementById('r31').addEventListener('input', getVal, false);
		document.getElementById('r32').addEventListener('input', getVal, false);
		document.getElementById('r33').addEventListener('input', getVal, false);
		
		document.getElementById('save-filter').addEventListener('click', saveFilter, false);
		canvas = document.getElementById('canvasTag');
		
		canvas_examples = document.getElementById('test_canvas');
		canvas_examples.addEventListener('click', savePhoto, false);

		document.getElementById('blr').addEventListener('input', setStyle, false);
		document.getElementById('grsc').addEventListener('input', setStyle, false);
		document.getElementById('brght').addEventListener('input', setStyle, false);
		document.getElementById('cntrst').addEventListener('input', setStyle, false);
		document.getElementById('huerot').addEventListener('input', setStyle, false);
		document.getElementById('invert').addEventListener('input', setStyle, false);
		document.getElementById('opc').addEventListener('input', setStyle, false);
		document.getElementById('str').addEventListener('input', setStyle, false);
		document.getElementById('sepia').addEventListener('input', setStyle, false);

		r11_e=document.getElementById('r11');
		r12_e=document.getElementById('r12');
		r13_e=document.getElementById('r13');
		r21_e=document.getElementById('r21');
		r22_e=document.getElementById('r22');
		r23_e=document.getElementById('r23');
		r31_e=document.getElementById('r31');
		r32_e=document.getElementById('r32');
		r33_e=document.getElementById('r33');


		savedfilters = json_savedfilters;

	navigator.getUserMedia({ video: true /*, audio: true */ }, initializeVideoStream, getUserMediaError);
};

var initializeVideoStream = function(stream) {
	mediaStream = stream;

	var video = document.getElementById('videoTag');
	if (typeof (video.srcObject) !== 'undefined') {
		video.srcObject = mediaStream;
	}
	else {
		video.src = URL.createObjectURL(mediaStream);
	}

};


var capture = function() {
	if (!mediaStream) {
		return;
	}


	var videoWidth = video.videoWidth;
	var videoHeight = video.videoHeight;

	if (canvas.width !== videoWidth || canvas.height !== videoHeight) {
		canvas.width  = videoWidth;
		canvas.height = videoHeight;
	}
	var ctx    = canvas.getContext('2d');
	setInterval(function() {
		if (video.paused || video.ended) return;
		ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
		canvasFilter(ctx, videoWidth, videoHeight);
	}, 15);

		//ctx.fillRect(0, 0, videoWidth, videoHeight);
	};

	var canvasFilter = function (ctx,videoWidth, videoHeight) {
		var imageData = ctx.getImageData(0, 0, videoWidth, videoHeight);
	//ctx.setTransform(r11,r12,r13,r21,r22,r23);
	ctx.putImageData(pixelChange(imageData,r11,r12,r13,r21,r22,r23,r31,r32,r33), 0, 0);
};

var pixelChange = function (imageData, q11,q12,q13,q21,q22,q23,q31,q32,q33,) {


	var pixels2 = imageData.data;
	var length = pixels2.length;
	var pixels = new Uint8Array(length);
	pixels = pixels2;
	var r = 0;
	var g = 0;
	var b = 0;
	for (var i = 0; i < length; i += 4) {
		r = pixels[i]-0;
		g = pixels[i + 1]-0;
		b = pixels[i + 2]-0;
			pixels[i] = (r * q11)+(g * q12)+(b * q13); // red
			pixels[i + 1] = (r * q21)+(g * q22)+(b * q23); // green
			pixels[i + 2] = (r * q31)+(g * q32)+(b * q33); // blue
		}

		pixels2 = pixels;

		return imageData;
};

var r11=0.5, r12=0.5,r13=0.5,
	r21=0.5,r22=0.5,r23=0.5,
	r31=0.5,r32=0.5, r33=0.5;

	var getVal=function () {

		r11=r11_e.value-0;
		r12=r12_e.value-0;
		r13=r13_e.value-0;

		r21=r21_e.value-0;
		r22=r22_e.value-0;
		r23=r23_e.value-0;

		r31=r31_e.value-0;
		r32=r32_e.value-0;
		r33=r33_e.value-0;
	};


	var changeExampFilter = function() {
		if (iter >= savedfilters.length - 1) {
			iter = 0;
		} else {
			iter++;
		}

		show_saved_filteres();
	};

	var savePhoto = function() {

		var imgData = canvas_examples.toDataURL('image/jpeg');
		var link = document.getElementById('saveImg');
		link.href = imgData;
		link.download = 'myPhoto.jpg';
		link.click();
	};
/*
	var resizeCanvas = function() {
		var resizer = document.getElementById('resize-id');
		resizer.style.transform = "scale(" + resize + ")";
		if (resize == 1) {
			resize = 0.5;
		} else {
			resize = 1;
		}
		
	};
*/
	var deleteExampFilter = function() {

		var request = new XMLHttpRequest();
		user_id = document.getElementById("user_id");

		var body = 'id=' + savedfilters[iter]['id'] + "&"
		+ 'user_id=' + user_id.innerHTML;
		console.log(body);
		request.open("POST", "/delete_filter", true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send(body);


	}


	var setExampFilter = function() {

		r11_e.value = savedfilters[iter]['r11'];
		r12_e.value = savedfilters[iter]['r12'];
		r13_e.value = savedfilters[iter]['r13'];
		r21_e.value = savedfilters[iter]['r21'];
		r22_e.value = savedfilters[iter]['r22'];
		r23_e.value = savedfilters[iter]['r23'];
		r31_e.value = savedfilters[iter]['r31'];
		r32_e.value = savedfilters[iter]['r32'];
		r33_e.value = savedfilters[iter]['r33'];

		getVal();

		blr=document.getElementById('blr');
		grsc=document.getElementById('grsc');
		brght=document.getElementById('brght');
		cntrst=document.getElementById('cntrst');
		huerot=document.getElementById('huerot');
		invert=document.getElementById('invert');
		opc=document.getElementById('opc');
		str=document.getElementById('str');
		sepia=document.getElementById('sepia');

		blr.value = savedfilters[iter]['blur'];
		grsc.value = savedfilters[iter]['grayscale'];
		brght.value = savedfilters[iter]['brightness'];
		cntrst.value = savedfilters[iter]['contrast'];
		huerot.value = savedfilters[iter]['hue_rotate'];
		invert.value = savedfilters[iter]['invert'];
		opc.value = savedfilters[iter]['opacity'];
		str.value = savedfilters[iter]['saturate'];
		sepia.value = savedfilters[iter]['sepia'];

		setStyle();
	};

	var show_saved_filteres = function() {
		var Width = video.videoWidth;
 		var Height = video.videoHeight;

		if (canvas_examples .width !== Width || canvas_examples.height !== Height) {
			canvas_examples.width  = Width;
			canvas_examples.height = Height;
		}

		var q11=savedfilters[iter]['r11'];
		var q12=savedfilters[iter]['r12'];
		var q13=savedfilters[iter]['r13'];
		var q21=savedfilters[iter]['r21'];
		var q22=savedfilters[iter]['r22'];
		var q23=savedfilters[iter]['r23'];
		var q31=savedfilters[iter]['r31'];
		var q32=savedfilters[iter]['r32'];
		var q33=savedfilters[iter]['r33'];

		var s=document.getElementsByClassName('grayscale');

		var blur = savedfilters[iter]['blur']
		var grayscale = savedfilters[iter]['grayscale']
		var brightness = savedfilters[iter]['brightness']
		var contrast = savedfilters[iter]['contrast']
		var hue_rotate = savedfilters[iter]['hue_rotate']
		var invert = savedfilters[iter]['invert']
		var opacity = savedfilters[iter]['opacity']
		var saturate = savedfilters[iter]['saturate']
		var sepia = savedfilters[iter]['sepia']

		var st = 'blur('+ blur + 'px)' + ' '
		+ 'grayscale(' +  grayscale + '%)' + ' '
		+ 'brightness(' + brightness + '%)' + ' '
		+ 'contrast(' +  contrast + '%)' + ' '
		+ 'hue-rotate(' +  hue_rotate + 'deg)' + ' '
		+ 'invert(' +  invert + '%)' + ' '
		+ 'opacity(' +  opacity + '%)' + ' '
		+ 'saturate(' +  saturate + '%)' + ' '
		+ 'sepia(' + sepia + '%)' + ' ';

		var can = document.getElementsByClassName("canvas_filtered");
		for (var i=0; i<s.length; i++) {
			can[i].style.WebkitFilter = st;
		};

		var ctx = canvas_examples.getContext('2d');
		ctx.drawImage(video, 0, 0, Width, Height);
		
		var imageData = ctx.getImageData(0, 0, Width, Height);
		//ctx.setTransform(r11,r12,r13,r21,r22,r23);
		ctx.putImageData(pixelChange(imageData,q11,q12,q13,q21,q22,q23,q31,q32,q33), 0, 0);
	};

	var writeError = function (string) {
		var elem = document.getElementById('error');
		var p    = document.createElement('div');
		p.appendChild(document.createTextNode('ERROR: ' + string));
		elem.appendChild(p);
	};

 	var saveFilter = function () {
		user_id = document.getElementById("user_id");
		var request = new XMLHttpRequest();

		var body = 'blur=' + blr.value + "&"
		+ 'grayscale=' + grsc.value + "&"
		+ 'brightness=' + brght.value + "&"
		+ 'contrast=' + cntrst.value + "&"
		+ 'hue_rotate=' + huerot.value + "&"
		+ 'invert=' + invert.value + "&"
		+ 'opacity=' + opc.value + "&"
		+ 'saturate=' + str.value + "&"
		+ 'sepia=' + sepia.value + "&";

		getVal();

		body += 'r11=' + r11 + "&"
		+ 'r12=' + r12 + "&"
		+ 'r13=' + r12 + "&"
		+ 'r21=' + r21 + "&"
		+ 'r22=' + r22 + "&"
		+ 'r23=' + r23 + "&"
		+ 'r31=' + r31 + "&"
		+ 'r32=' + r32 + "&"
		+ 'r33=' + r33 + "&"
		+ 'user_id=' + user_id.innerHTML;

		console.log(body);
		request.open("POST", "/video_feed", true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send(body);
	};

	var getUserMediaError = function (e) {
		if (e.name.indexOf('NotFoundError') >= 0) {
			writeError('Webcam not found.');
		}
		else {
			writeError('The following error occurred: "' + e.name + '" Please check your webcam device(s) and try again.');
		}
	};

	var setStyle = function () {
		
		var blr=document.getElementById('blr');
		var grsc=document.getElementById('grsc');
		var brght=document.getElementById('brght');
		var cntrst=document.getElementById('cntrst');
		var huerot=document.getElementById('huerot');
		var invert=document.getElementById('invert');
		var opc=document.getElementById('opc');
		var str=document.getElementById('str');
		var sepia=document.getElementById('sepia');
		var s=document.getElementsByClassName('grayscale');

		var st = 'blur('+ blr.value + 'px)' + ' '
		+ 'grayscale(' + grsc.value + '%)' + ' '
		+ 'brightness(' + brght.value + '%)' + ' '
		+ 'contrast(' + cntrst.value + '%)' + ' '
		+ 'hue-rotate(' + huerot.value + 'deg)' + ' '
		+ 'invert(' + invert.value + '%)' + ' '
		+ 'opacity(' + opc.value + '%)' + ' '
		+ 'saturate(' + str.value + '%)' + ' '
		+ 'sepia(' + sepia.value + '%)' + ' ';

		console.log(st);

		for (var i=0;i<s.length;i++) {
			s[i].style.WebkitFilter = st;
		};
	}

	init();
}());
