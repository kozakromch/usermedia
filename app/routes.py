# -*- coding: utf-8 -*-

from flask import render_template, flash, redirect, url_for, request, abort
from app import app, db
from app.forms import LoginForm, RegistrationForm
from flask_login import current_user, login_user, login_required
from app.models import User, Post
from flask_login import logout_user
from werkzeug.urls import url_parse
from json import dumps
from flask_sqlalchemy  import get_debug_queries
import time
from app.email import generate_confirmation_token, confirm_token, send_email




@app.teardown_request
def teardown_request(exception=None):
	query_log = open('sql_query_log_file', 'a') 
	query_log.write(str(time.time) + ' :  ' + str(get_debug_queries()))
	query_log.flush()
	query_log.close()


@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404


@app.route('/')
@app.route('/index/')
@login_required
def index():
	return render_template('index.html', title='Home')

@app.route('/video_feed', methods=['GET', 'POST'])
@login_required
def video_feed():

	if request.method == 'POST':
		p = Post()
		try:
			p.r11 = request.form['r11']
			p.r12 = request.form['r12']
			p.r13 = request.form['r13']
			p.r21 = request.form['r21']
			p.r22 = request.form['r22']
			p.r23 = request.form['r23']
			p.r31 = request.form['r31']
			p.r32 = request.form['r32']
			p.r33 = request.form['r33']
			p.blur = request.form['blur']
			p.grayscale = request.form['grayscale']
			p.brightness = request.form['brightness']
			p.contrast = request.form['contrast']
			p.hue_rotate = request.form['hue_rotate']
			p.invert = request.form['invert']
			p.opacity = request.form['opacity']
			p.saturate = request.form['saturate']
			p.sepia = request.form['sepia']
			p.user_id = current_user.id
		except (KeyError, BadRequest) as e:
			abort(404)
			if hasattr(e, 'message'):
				abort(Response(e.message))
			else:
				abort(Response(e))
		else:
			db.session.add(p)
			db.session.commit()




		return redirect(url_for('user', username=current_user.username))

	return render_template('index.html', title='Home', user = user )


@app.route('/delete_filter', methods=['GET', 'POST'])
@login_required
def delete_filter():
		if request.method == 'POST':
			user = User.query.filter_by(id=request.form['user_id']).first()
			p = Post.query.filter_by(id=request.form['id']).first_or_404()
			db.session.delete(p)
			db.session.commit()
		return redirect(url_for('user', username=user.username))


@app.route('/login', methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('index'))

	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(username=form.username.data).first()
		if user is None or not user.check_password(form.password.data):
			flash('Invalid username or password')
			return redirect(url_for('login'))
		if user.is_enabled == 0:
			flash('Confirm your email')
			form=RegistrationForm()
			return render_template('resend_confirmation', form=form)
		login_user(user, remember=form.remember_me.data)
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc != '':
			next_page = url_for('index')
		return redirect(next_page)

	return render_template('login.html', title='Sign In', form=form)


@app.route('/resend_confirmation')
def resend_confirmation():
	if current_user.is_authenticated:
		return redirect(url_for('index'))

	form = RegistrationForm()
	user = User.query.filter_by(username=form.email.data).first()
	if user is None:
		flash("No user with such email")
		return redirect(url_for('login'))
	if user.is_enabled == 1:
		flash("This user is already confirmed")
		return redirect(url_for('login'))
	else:
		send_email(form)
		token = generate_confirmation_token(user.email)
		confirm_url = url_for('confirm_email', token=token, _external=True)
		html = render_template('activate.html', confirm_url=confirm_url)
		subject = "Please confirm your email"
		send_email(current_user.email, subject, html)
		flash('A new confirmation email has been sent.', 'success')
		return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('index'))
	form = RegistrationForm()
	if form.validate_on_submit():
		user = User(username=form.username.data, email=form.email.data, created_at=time.time(), is_enabled=0)
		user.set_password(form.password.data)
		db.session.add(user)
		db.session.commit()


		token = generate_confirmation_token(user.email)

		confirm_url = url_for('confirm_email', token=token, _external=True)
		html = render_template('activate.html', confirm_url=confirm_url)
		subject = "confirmation"
		send_email(user.email, subject, html)

		flash('Confirmation send to your email')
		return redirect(url_for('login'))
		
	return render_template('register.html', title='Register', form=form)

@app.route('/logout')
def logout():
	logout_user()
	return redirect(url_for('login'))


@app.route('/user/<username>')
@login_required
def user(username):
	if (current_user.username== username):
		user = User.query.filter_by(username=username).first_or_404()
		posts = user.self_post()
		filters = []
		for post in posts:
			filters.append(post.make_dict())
		return render_template('user.html', user=user, filters=filters)
	return redirect(url_for('login'))



@app.route('/confirm/<token>')
def confirm_email(token):
	try:
		email = confirm_token(token)
	except:
		flash('The confirmation link is invalid or has expired.', 'danger')
	user = User.query.filter_by(email=email).first_or_404()
	if user.is_enabled == 1:
		flash('Account already confirmed. Please login.', 'success')
	else:
		user.is_enabled = True
		user.confirmed_on = time.time()
		db.session.add(user)
		db.session.commit()
		flash('You have confirmed your account', 'success')
	return redirect(url_for('login'))

	
if __name__ == '__main__':
	app.run(host='0.0.0.0', threaded=True, debug=True)