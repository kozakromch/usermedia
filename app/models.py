from datetime import datetime
from app import db, login, login_serializer, app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from json import dumps

class User(UserMixin, db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), index=True, unique=True)
	email = db.Column(db.String(120), index=True, unique=True)
	password_hash = db.Column(db.String(128))
	posts = db.relationship('Post', backref='author', lazy='dynamic')
	created_at = db.Column(db.Integer)
	is_enabled = db.Column(db.Integer)

	def get_auth_token(self):
		data = [str(self.id), self.password_hash]
		return login_serializer.dumps(data)

	def get_id(self):
		return self.get_auth_token()

	def set_password(self, password):
		self.password_hash = generate_password_hash(password)

	def check_password(self, password):
		return check_password_hash(self.password_hash, password)


	def self_post(self):
		return Post.query.filter(self.id == Post.user_id).all()


	def __repr__(self):
		return '<User {}>'.format(self.username)

@login.user_loader
def load_user(token):
	max_age = app.config["REMEMBER_COOKIE_DURATION"].total_seconds()
	data = login_serializer.loads(token, max_age=max_age)
	user = User.query.filter_by(id = (data[0])).first()
	if user and data[1] == user.password_hash:
		return user
	return None

# @login.user_loader
# def load_user(id):
# 	return User.query.get(int(id))
 

class Post(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	r11 = db.Column(db.Float)
	r12 = db.Column(db.Float)
	r13 = db.Column(db.Float)
	r21 = db.Column(db.Float)
	r22 = db.Column(db.Float)
	r23 = db.Column(db.Float)
	r31 = db.Column(db.Float)
	r32 = db.Column(db.Float)
	r33 = db.Column(db.Float)
	blur = db.Column(db.Integer)
	grayscale = db.Column(db.Integer)
	brightness = db.Column(db.Integer)
	contrast = db.Column(db.Integer)
	hue_rotate = db.Column(db.Integer)
	invert = db.Column(db.Integer)
	opacity = db.Column(db.Integer)
	saturate = db.Column(db.Integer)
	sepia = db.Column(db.Integer)

	timestamp = db.Column(db.DateTime, index = True, default = datetime.utcnow)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

	def __repr__(self):
		return str(self.make_dict())
	
	def make_dict(self):
		return {

			'id' : self.id,
			'r11' : self.r11,
			'r12' : self.r12,
			'r13' : self.r13,
			'r21' : self.r21,
			'r22' : self.r22,
			'r23' : self.r23,
			'r31' : self.r31,
			'r32' : self.r32,
			'r33' : self.r33,
			'blur' : self.blur,
			'grayscale' : self.grayscale,
			'brightness' : self.brightness,
			'contrast' : self.contrast,
			'hue_rotate' : self.hue_rotate,
			'invert' : self.invert,
			'opacity' : self.opacity,
			'saturate' : self.saturate,
			'sepia' : self.sepia,
								}


