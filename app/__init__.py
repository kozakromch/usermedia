from flask import Flask, url_for
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_mail import Mail
from flask_paranoid import Paranoid
from itsdangerous import URLSafeTimedSerializer


app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

login_serializer = URLSafeTimedSerializer(app.secret_key)
login = LoginManager(app)
login.login_view = 'login'

mail = Mail(app)

paranoid = Paranoid(app)
paranoid.redirect_view = '/'

from app import routes, models