from flask_mail import Message
from app import mail, app
from itsdangerous import URLSafeTimedSerializer



def generate_confirmation_token(email):
	serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
	return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=3600):
	serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
	try:
		email = serializer.loads(
			token,
			salt=app.config['SECURITY_PASSWORD_SALT'],
			max_age=expiration
		)
	except:
		return False
	return email

def send_email(to, subject, template):
	with mail.connect() as conn:
		msg = Message(
			subject,
			recipients=[to],
			html=template,
			sender='flaskusermedia@gmail.com'
		)
		conn.send(msg)