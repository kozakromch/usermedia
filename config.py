import os 
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
	SECRET_KEY = os.environ.get('SECRET_KEY') or '123'
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
		'sqlite:///' + os.path.join(basedir, 'app.db')
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	MAIL_SERVER = 'smtp.googlemail.com'
	MAIL_PORT = 465
	MAIL_USE_TLS = False
	MAIL_USE_SSL = True
	MAIL_USERNAME = 'flaskusermedia'
	MAIL_PASSWORD = 'Social1234'
	SECURITY_EMAIL_SENDER = 'flaskusermedia@gmail.com'
	SECURITY_PASSWORD_SALT = 'my_precious_two'
	REMEMBER_COOKIE_DURATION = timedelta(seconds=120)
	#WTF 
	# SESSION_COOKIE_SECURE = True
	# REMEMBER_COOKIE_SECURE = True
	# SESSION_COOKIE_HTTPONLY = True
	# REMEMBER_COOKIE_HTTPONLY = True